package cn.ijero.easytoolbar

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.view.updateLayoutParams
import cn.ijero.easytoolbar.databinding.LayoutEasyToolbarBinding
import cn.ijero.easytoolbar.style.TextStyle

class EasyToolbar
@JvmOverloads
constructor(context: Context?, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    RelativeLayout(context, attrs, defStyleAttr) {

    private var leftListener: OnClickListener? = null
    private var rightListener: OnClickListener? = null
    private var titleListener: OnClickListener? = null

    var showLeftIcon = true
        private set
    var showRightIcon = false
        private set
    var showTitle = true
        private set
    var leftIcon: Drawable? = null
        private set
    var rightIcon: Drawable? = null
        private set
    var title: String? = null
        private set

    var contentInsetStart = 0f
        private set
    var contentInsetEnd = 0f
        private set

    val titleTextView: TextView
        get() = binding.titleTextView

    val leftImageView: ImageView
        get() = binding.leftImageView

    val rightImageView: ImageView
        get() = binding.rightImageView

    var titleTextStyle = Typeface.NORMAL
        private set

    private var titleTextSize = 0f
    private var titleTextColor = Color.BLACK

    private val defTitleTextSize by lazy {
        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 16f, resources.displayMetrics)
    }

    private val binding = LayoutEasyToolbarBinding.bind(LayoutInflater.from(context)
        .inflate(R.layout.layout_easy_toolbar, this, true))

    init {
        applyStyle(attrs)
        setDefValues()
    }

    private fun applyStyle(attrs: AttributeSet?) {
        val ta = context.obtainStyledAttributes(attrs, R.styleable.EasyToolbar)
        leftIcon = ta.getDrawable(R.styleable.EasyToolbar_et_leftIcon)
        rightIcon = ta.getDrawable(R.styleable.EasyToolbar_et_rightIcon)
        showLeftIcon = ta.getBoolean(R.styleable.EasyToolbar_et_showLeftIcon, showLeftIcon)
        showRightIcon = ta.getBoolean(R.styleable.EasyToolbar_et_showRightIcon, showRightIcon)
        contentInsetStart =
            ta.getDimension(R.styleable.EasyToolbar_et_contentInsetStart, contentInsetStart)
        contentInsetEnd =
            ta.getDimension(R.styleable.EasyToolbar_et_contentInsetEnd, contentInsetEnd)
        showTitle = ta.getBoolean(R.styleable.EasyToolbar_et_showTitle, showTitle)
        title = ta.getString(R.styleable.EasyToolbar_et_title)
        titleTextStyle = ta.getInt(R.styleable.EasyToolbar_et_titleTextStyle, titleTextStyle)
        titleTextSize = ta.getDimension(R.styleable.EasyToolbar_et_titleTextSize, defTitleTextSize)
        titleTextColor = ta.getColor(R.styleable.EasyToolbar_et_titleTextColor, titleTextColor)
        ta.recycle()
    }

    private fun setDefValues() {
        showLeft()
        showRight()
        showTitle()
        setTitle()
        setTitleTextStyle()
        setTitleTextSize()
        setTextColor()
        setLeftIcon()
        setRightIcon()
        setListeners()
        contentInsets(contentInsetStart, contentInsetEnd)
    }

    private fun setTitleTextStyle() {
        titleTextView.setTypeface(null, titleTextStyle)
    }

    private fun setListeners() {
        leftImageView.setOnClickListener {
            leftListener?.onClick(it)
        }
        rightImageView.setOnClickListener {
            rightListener?.onClick(it)
        }
        titleTextView.setOnClickListener {
            titleListener?.onClick(it)
        }
    }

    private fun setRightIcon() {
        rightIcon?.let {
            rightImageView.setImageDrawable(it)
        }
    }

    private fun setLeftIcon() {
        leftIcon?.let {
            leftImageView.setImageDrawable(it)
        }
    }

    private fun setTitle() {
        titleTextView.text = title
    }

    private fun setTitleTextSize() {
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, titleTextSize)
    }

    private fun setTextColor() {
        titleTextView.setTextColor(titleTextColor)
    }

    private fun showTitle() {
        titleTextView.isVisible = showTitle
    }

    private fun showRight() {
        rightImageView.isVisible = showRightIcon
    }

    private fun showLeft() {
        leftImageView.isVisible = showLeftIcon
    }

    /**
     *
     * 设置点击监听器
     * @param leftListener 左边icon的点击监听器
     * @param rightListener 右边icon的点击监听器
     * @param titleListener  标题的点击监听器
     *
     **/
    fun clickListeners(
        leftListener: OnClickListener? = null,
        rightListener: OnClickListener? = null,
        titleListener: OnClickListener? = null,
    ): EasyToolbar {
        this.leftListener = leftListener
        this.rightListener = rightListener
        this.titleListener = titleListener
        return this
    }


    /**
     *
     * 单独设置左边按钮点击监听器
     * @param leftListener 左边icon的点击监听器
     *
     **/
    fun clickLeftListener(
        leftListener: OnClickListener? = null,
    ): EasyToolbar {
        this.leftListener = leftListener
        return this
    }

    /**
     *
     * 单独设置右边按钮点击监听器
     * @param rightListener 右边icon的点击监听器
     *
     **/
    fun clickRightListener(
        rightListener: OnClickListener? = null,
    ): EasyToolbar {
        this.rightListener = rightListener
        return this
    }

    /**
     *
     * 单独设置标题点击监听器
     * @param titleListener 标题的点击监听器
     *
     **/
    fun clickTitleListener(
        titleListener: OnClickListener? = null,
    ): EasyToolbar {
        this.titleListener = titleListener
        return this
    }

    /**
     *
     * 是否显示标题
     *
     * @param isShow true - 显示 ， false - 不显示
     **/
    fun showTitle(isShow: Boolean): EasyToolbar {
        this.showTitle = isShow
        showTitle()
        return this
    }

    /**
     *
     * 是否显示左边按钮
     *
     * @param isShow true - 显示 ， false - 不显示
     **/
    fun showLeftIcon(isShow: Boolean): EasyToolbar {
        this.showLeftIcon = isShow
        showLeft()
        return this
    }

    /**
     *
     * 是否显示右边按钮
     *
     * @param isShow true - 显示 ， false - 不显示
     **/
    fun showRightIcon(isShow: Boolean): EasyToolbar {
        this.showRightIcon = isShow
        showRight()
        return this
    }

    /**
     *
     * 设置标题
     * @param title 标题文本内容
     *
     **/
    fun title(title: String?): EasyToolbar {
        this.title = title
        setTitle()
        return this
    }

    /**
     *
     * 设置标题
     * @param resId 文本资源id
     *
     **/
    fun title(@StringRes resId: Int): EasyToolbar {
        this.title = context.getString(resId)
        setTitle()
        return this
    }


    /**
     *
     * 设置标题文字颜色
     * @param color 文字颜色值
     **/
    fun titleTextColor(@ColorInt color: Int): EasyToolbar {
        this.titleTextColor = color
        setTextColor()
        return this
    }

    /**
     *
     * 设置标题文字颜色
     * @param resId 颜色的资源id
     **/
    fun titleTextColorRes(@ColorRes resId: Int): EasyToolbar {
        this.titleTextColor = context.getColor(resId)
        setTextColor()
        return this
    }

    /**
     *
     * 设置标题文字字体大小
     * @param textSize 字体大小
     **/
    fun titleTextSize(textSize: Float): EasyToolbar {
        this.titleTextSize = textSize
        setTitleTextSize()
        return this
    }

    /**
     *
     * 设置标题右边icon图标Drawable
     * @param icon 图标Drawable，null 表示不设置
     **/
    fun rightIcon(icon: Drawable?): EasyToolbar {
        this.rightIcon = icon
        setRightIcon()
        return this
    }

    /**
     *
     * 设置标题右边icon图标的资源id
     * @param resId 图标的资源id
     **/
    fun rightIcon(@DrawableRes resId: Int): EasyToolbar {
        this.rightIcon = ContextCompat.getDrawable(context, resId)
        setRightIcon()
        return this
    }

    /**
     *
     * 设置标题左边icon图标Drawable
     * @param icon 图标Drawable，null 表示不设置
     **/
    fun leftIcon(icon: Drawable?): EasyToolbar {
        this.leftIcon = icon
        setLeftIcon()
        return this
    }

    /**
     *
     * 设置标题左边icon图标的资源id
     * @param resId 图标的资源id
     **/
    fun leftIcon(@DrawableRes resId: Int): EasyToolbar {
        this.leftIcon = ContextCompat.getDrawable(context, resId)
        setLeftIcon()
        return this
    }

    /**
     *
     * 设置icon边距
     *
     * @param start 左边距
     * @param end 右边距
     *
     * @see [contentInsetStart]
     * @see [contentInsetEnd]
     **/
    fun contentInsets(start: Float = 0f, end: Float = 0f): EasyToolbar {
        contentInsetStart(start)
        contentInsetEnd(end)
        return this
    }

    /**
     *
     * 设置icon右边距
     *
     * @param end 右边距
     *
     * @see [contentInsets]
     **/
    fun contentInsetEnd(end: Float): EasyToolbar {
        this.contentInsetEnd = end
        rightImageView.updateLayoutParams<LayoutParams> {
            rightMargin = end.toInt()
        }
        return this
    }

    /**
     *
     * 设置icon左边距
     *
     * @param start 左边距
     *
     * @see [contentInsets]
     *
     **/
    fun contentInsetStart(start: Float): EasyToolbar {
        this.contentInsetStart = start
        leftImageView.updateLayoutParams<LayoutParams> {
            leftMargin = start.toInt()
        }
        return this
    }

    /**
     *
     * 设置标题文字字体样式
     *
     * @param style [TextStyle.NORMAL] - 正常字体， [TextStyle.BOLD] - 粗体
     *
     **/
    fun titleTextStyle(style: TextStyle): EasyToolbar {
        this.titleTextStyle = style.style
        setTitleTextStyle()
        return this
    }

}