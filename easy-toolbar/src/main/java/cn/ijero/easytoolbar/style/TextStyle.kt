package cn.ijero.easytoolbar.style

import android.graphics.Typeface

enum class TextStyle(val style: Int) {
    NORMAL(Typeface.NORMAL),
    BOLD(Typeface.BOLD)
}