package cn.ijero.easytoolbar

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.widget.Toast
import androidx.core.content.ContextCompat
import cn.ijero.easytoolbar.databinding.ActivityMainBinding
import cn.ijero.easytoolbar.style.TextStyle

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val backDrawable = ContextCompat.getDrawable(this, R.drawable.ic_easy_toolbar_back)
        val searchDrawable = ContextCompat.getDrawable(this, R.drawable.ic_easy_toolbar_search)

        val titleTextSize =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14f, resources.displayMetrics)

        binding.toolbar
            .showLeftIcon(true)
            .showRightIcon(true)
            .leftIcon(R.drawable.ic_easy_toolbar_back)
            .leftIcon(backDrawable)
            .rightIcon(R.drawable.ic_easy_toolbar_search)
            .rightIcon(searchDrawable)
            .showTitle(true)
            .title("首页")
            .title(R.string.app_name)
            .titleTextColorRes(R.color.et_titleText)
            .titleTextColor(Color.BLACK)
            .titleTextSize(titleTextSize)
            .titleTextStyle(TextStyle.BOLD)
            .clickListeners(
                {
                    // leftIcon
                    Toast.makeText(this@MainActivity, "返回", Toast.LENGTH_SHORT).show()
                }, {
                    // rightIcon
                    Toast.makeText(this@MainActivity, "搜索", Toast.LENGTH_SHORT).show()
                }, {
                    // title
                    Toast.makeText(this@MainActivity, "标题", Toast.LENGTH_SHORT).show()
                }
            )
            .clickLeftListener {
                // 点击leftIcon
            }
            .clickRightListener {
                // 点击rightIcon
            }
            .clickTitleListener {
                // 点击title
            }
    }
}