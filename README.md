# 简单Toolbar - EasyToolbar

## 快速配置

- 项目根目录build.gradle

```grovvy
repositories {
    google()
    mavenCentral()
	// 加入此行
    maven {
        url 'https://repo1.maven.org/maven2/'
    }
}
```

- app目录build.gralde

```grovvy
dependencies {
	// ...
	implementation 'io.gitee.ijero:easy-toolbar:0.0.3'
}
```

- xml配置示例

```xml
<cn.ijero.easytoolbar.EasyToolbar
    android:id="@+id/toolbar"
    android:layout_width="match_parent"
    android:layout_height="?actionBarSize"
    app:et_contentInsetEnd="6dp"
    app:et_contentInsetStart="0dp"
    app:et_leftIcon="@drawable/ic_easy_toolbar_back"
    app:et_rightIcon="@drawable/ic_easy_toolbar_search"
    app:et_showLeftIcon="true"
    app:et_showRightIcon="true"
    app:et_showTitle="true"
    app:et_title="这是标题"
    app:et_titleTextColor="#FF0000"
    app:et_titleTextSize="18sp"
    app:et_titleTextStyle="bold"
    app:layout_constraintTop_toTopOf="parent" />
```

- 代码配置示例：

```kotlin
binding.toolbar
    .showLeftIcon(true)
    .showRightIcon(true)
    .leftIcon(R.drawable.ic_easy_toolbar_back)
    .leftIcon(backDrawable)
    .rightIcon(R.drawable.ic_easy_toolbar_search)
    .rightIcon(searchDrawable)
    .showTitle(true)
    .title("首页")
    .title(R.string.app_name)
    .titleTextColorRes(R.color.et_titleText)
    .titleTextColor(Color.BLACK)
    .titleTextSize(titleTextSize)
    .titleTextStyle(TextStyle.BOLD)
    .clickListeners(
        {
            // leftIcon
            Toast.makeText(this@MainActivity, "返回", Toast.LENGTH_SHORT).show()
        }, {
            // rightIcon
            Toast.makeText(this@MainActivity, "搜索", Toast.LENGTH_SHORT).show()
        }, {
            // title
            Toast.makeText(this@MainActivity, "标题", Toast.LENGTH_SHORT).show()
        }
    )
    .clickLeftListener {
        // 点击leftIcon
    }
    .clickRightListener {
        // 点击rightIcon
    }
    .clickTitleListener {
        // 点击title
    }
```

**类方法介绍请看代码注释：[EasyToolbar.kt](./easy-toolbar/src/main/java/cn/ijero/easytoolbar/EasyToolbar.kt)**

## 详细属性介绍

```xml
    <!--左边图标-->
    <attr name="et_leftIcon" format="reference" />
    <!--右边图标-->
    <attr name="et_rightIcon" format="reference" />
    <!--显示左边图标-->
    <attr name="et_showLeftIcon" format="boolean" />
    <!--左边icon边距-->
    <attr name="et_contentInsetStart" format="dimension" />
    <!--右边icon边距-->
    <attr name="et_contentInsetEnd" format="dimension" />
    <!--显示右边图标-->
    <attr name="et_showRightIcon" format="boolean" />
    <!--显示标题-->
    <attr name="et_showTitle" format="boolean" />
    <!--标题-->
    <attr name="et_title" format="string" />
    <!--标题颜色-->
    <attr name="et_titleTextColor" format="color" />
    <!--标题字体大小-->
    <attr name="et_titleTextSize" format="dimension" />
    <!--标题字体样式：normal - 标准 ，bold - 粗体-->
    <attr name="et_titleTextStyle">
        <flag name="normal" value="0" />
        <flag name="bold" value="1" />
    </attr>
```
